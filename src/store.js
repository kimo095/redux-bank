// import {createStore , combineReducers , applyMiddleware} from "redux";   
// import accountReducer from "./features/accounts/accountSlice";
// import customerReducer from "./features/customers/customerSlice";
// import { composeWithDevTools } from "redux-devtools-extension";
// import {thunk} from 'redux-thunk';

// const rootReducer = combineReducers({
//     account:accountReducer,
//     customer:customerReducer
// })

// const store = createStore(rootReducer, composeWithDevTools (applyMiddleware(thunk)));


// store.dispatch({type:"account/deposit" , payload:500});
// store.dispatch({type:"account/withdraw" , payload:200});
// console.log(store.getState());

// store.dispatch({type:"account/requestLoan" , payload:{
//     amount:1000 , purpose:"Buy a car"
// }});
// console.log(store.getState());
// store.dispatch({type:"account/payLoan"});
// console.log(store.getState());

// export default store;

//using redux toolkit instead

import {configureStore} from "@reduxjs/toolkit";
import accountReducer from "./features/accounts/accountSlice";
import customerReducer from "./features/customers/customerSlice";


const store = configureStore({
    reducer:{
        account:accountReducer,
        customer:customerReducer
    }
});
export default store;



