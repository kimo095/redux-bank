// const intialStateCustomer = {
//     fullName:"" ,
//     nationalID:"",
//     CreatedAt:""
// }

// export default function customerReducer(state=intialStateCustomer , action){
//     switch(action.type){
//         case "customer/createCustomer":
//             return {...state,
//                      fullName:action.payload.fullName,
//                      nationalID:action.payload.nationalID,
//                      createdAt:action.payload.createdAt
//                     };
//         case "customer/updateName":
//             return {...state, 
//                     fullName:action.payload
//                     };
//         default:
//             return state;
//     }

// }
// //function creator for customers

// export function createCustomer(fullName, nationalID){
//     return {
//         type:"customer/createCustomer",
//         payload:{
//             fullName,nationalID,createdAt:new Date().toISOString()
//         }
//     }
// }
// export function updateName(fullName){
//     return {
//         type:"customer/updateName",
//         payload:fullName
//     }
// }

// store.dispatch(createCustomer("Abdelkareem Jebreel" , "123sse456643"));
// store.dispatch(deposit(250));
// console.log(store.getState());


import {createSlice} from "@reduxjs/toolkit"
import { payLoan } from "../accounts/accountSlice";

const intialState= {
    fullName:"" ,
    nationalID:"",
    CreatedAt:""
}

const customerSlice = createSlice({
    name:'customer',
    initialState:intialState,
    reducers:{
        createCustomer:{
            prepare(fullName,nationalID){
                return{
                    payload:{
                        fullName,
                        nationalID,
                        createdAt:new Date().toISOString(),
                    },
                };
            },
            reducer(state, action){
            state.fullName= action.payload.fullName;
            state.nationalID= action.payload.nationalID;
            state.CreatedAt= action.payload.CreatedAt;
        },
    },
        updateName(state,action){
            state.fullName= action.payload
        },
    },
});

export const {createCustomer,updateName} = customerSlice.actions;

export default customerSlice.reducer;